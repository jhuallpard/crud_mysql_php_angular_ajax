<?php
header("Access-Control-Allow-Origin: http://localhost:4200");
if (empty($_GET["idPositions"])) {
    exit("No hay id de positions");
}
$idPositions = $_GET["idPositions"];
$bd = include_once "bd.php";
$sentencia = $bd->prepare("select id, name, description, status, created, modified from positions where id = ?");
$sentencia->execute([$idPositions]);
$positions = $sentencia->fetchObject();
echo json_encode($positions);
