
<?php
header("Access-Control-Allow-Origin: http://localhost:4200");
header("Access-Control-Allow-Methods: PUT");
header("Access-Control-Allow-Headers: *");
if ($_SERVER["REQUEST_METHOD"] != "PUT") {
    exit("Solo acepto peticiones PUT");
}
$jsonMenus = json_decode(file_get_contents("php://input"));
if (!$jsonMenus) {
    exit("No hay datos");
}
$bd = include_once "bd.php";
$sentencia = $bd->prepare("UPDATE menus SET title = ?, description = ?, status = ?, created = ?, modified = ?, father_id = ?, link = ?, position_id = ? WHERE id = ?");
$resultado = $sentencia->execute([$jsonMenus->title, $jsonMenus->description, $jsonMenus->status, $jsonMenus->created, $jsonMenus->modified, $jsonMenus->father_id, $jsonMenus->link, $jsonMenus->position_id, $jsonMenus->id]);
echo json_encode($resultado);
