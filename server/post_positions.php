<?php
header("Access-Control-Allow-Origin: http://localhost:4200");
header("Access-Control-Allow-Headers: *");
$jsonPositions = json_decode(file_get_contents("php://input"));
if (!$jsonPositions) {
    exit("No hay datos");
}
$bd = include_once "bd.php";
$sentencia = $bd->prepare("insert into positions(name, description, status, created, modified) values (?,?,?,?,?)");
$resultado = $sentencia->execute([$jsonPositions->name, $jsonPositions->description, $jsonPositions->status, $jsonPositions->created, $jsonPositions->modified]);
echo json_encode([
    "resultado" => $resultado,
]);
