import { Component, OnInit, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import { Positions } from 'src/app/models/positions';
import { PositionsService } from "src/app/services/positions.service"

export interface DialogData {
  status: number;
}
@Component({
  selector: 'app-dialog-status',
  templateUrl: './dialog-status.component.html',
  styleUrls: ['./dialog-status.component.css']
})
export class DialogStatusComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DialogStatusComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  	onNoClick(): void {
      this.dialogRef.close();
    }


  ngOnInit() {
  }
  positionsModel = new Positions("", "", undefined, new Date(),new Date())


}
