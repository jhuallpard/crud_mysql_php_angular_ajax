
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarMenusComponent } from './editar-menus.component';

describe('EditarMenusComponent', () => {
  let component: EditarMenusComponent;
  let fixture: ComponentFixture<EditarMenusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarMenusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarMenusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
