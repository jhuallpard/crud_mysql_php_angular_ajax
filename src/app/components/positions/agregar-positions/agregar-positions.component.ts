
import { Component, OnInit, Inject } from '@angular/core';
import { Positions } from 'src/app/models/positions';
import { PositionsService } from "src/app/services/positions.service"
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { DialogStatusComponent } from "src/app/components/dialog-status/dialog-status.component"

export interface DialogData {
  status: number;
}
/**
 * @title Dialog Overview
 */

 @Component({
  selector: 'app-agregar-positions',
  templateUrl: './agregar-positions.component.html',
  styleUrls: ['./agregar-positions.component.css']
})
export class AgregarPositionsComponent implements OnInit {
    status: number;

  constructor(private positionsService: PositionsService, private snackBar: MatSnackBar, private router: Router, public dialog: MatDialog) { }
  
  
  ngOnInit() {
  }
  positionsModel = new Positions("", "", undefined, new Date(),new Date())

  onSubmit() {
    this.positionsService.addPosition(this.positionsModel).subscribe(() => {
      this.snackBar.open('Position guardada', undefined, {
        duration: 1500,
      });
      this.router.navigate(['/positions']);
    })
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogStatusComponent, {
      width: '250px',
      data: {status: this.status}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.status = result;
    });
  }

}



