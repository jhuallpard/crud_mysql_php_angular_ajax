export class Consulta {
    constructor(
        public descriptionP: string,
        public descriptionM: string,
        public father_id: number,
        public link: string,
        public position_id: number,
        public id?: number,
    ) { }

}