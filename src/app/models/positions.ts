export class Positions {
    constructor(
        public name: string,
        public description: string,
        public status: number,
        public created: Date,
        public modified: Date,
        public id?: number,
    ) { }

}
